# immutant-cljs-debugging

Test case for immutant issue when requiring new clojurescript libraries.

## Reproducing ##

  0. Start cljsbuild running via `lein do cljsbuild clean, cljsbuild auto`
  1. Start the app running under immutant (`lein immutant run` or `lein immutant
     deploy` as appropriate)
  2. Go to [http://localhost:8080/debugging](), open the console, and observer
     "Hello World" logged with no errors.
  3. Open `core.cljs`, and remove the `#_` comments on the require and second
     log
  4. After cljs compilation is finished, reload the page
  5. Errors appear in the console about not being able to locate the `goog.crypt`
     library, even though the files can be seen in `resources/public/js/goog`
  6. Once immutant is restarted, then the files are located properly.
