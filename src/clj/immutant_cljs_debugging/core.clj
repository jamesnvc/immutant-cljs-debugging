(ns immutant-cljs-debugging.core
  (:require [compojure.core :refer :all]
            [compojure.handler :as handler]
            [compojure.route :as route]))

(defroutes app-routes
  (GET "/" []
    "<html>
    <head>
    </head>
    <body>
    <div id='app'>Bloop</div>
    <script src='./js/goog/base.js'></script>
    <script src='./js/debugging.js'></script>
    <script>goog.require('immutant_cljs_debugging.core');</script>
    </body>
    </html>"))

(def app (handler/site
           (routes
             app-routes
             (route/resources "/"))))
