(ns immutant.init
  (:require [immutant.web :as web]
            [immutant-cljs-debugging.core :refer [app]]))

(web/start app)
