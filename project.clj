(defproject immutant-cljs-debugging "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :source-paths ["src/clj"]
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2202"]
                 [compojure "1.1.6"]]

  :plugins [[lein-cljsbuild "1.0.3"]]

  :immutant {:context-path "/debugging"}
  :cljsbuild {:builds
              [{:source-paths ["src/cljs"]
                :compiler {:output-dir "resources/public/js"
                           :output-to "resources/public/js/debugging.js"
                           :optimizations :none
                           :source-maps true}}]})
